# cheatsheet

## avoid bip beep sound hardware speaker in xfce blacklist it in debian
```
	sudo subl /etc/modprobe.d/blacklist.conf
	add
	# this is how you mute annoying beeps in console and shutdown
	blacklist pcspkr
```
## avoid bip beep sound hardware speaker globally in manjaro
```
	temporarilly: sudo rmmod pcspkr
	permanently: 
		sudo echo "blacklist pcspkr" > /etc/modprobe.d/nobeep.conf
```
## aws cli uninstall
```
	see downloaded html “aws uninstall”
```
## back up phone's albums
to include only the images and videos under directories starting with "202*",
in grsync -> advanced options -> Additional options and fill in:
```
--include='*.jpg'
--include='*.mp4'
--include='202*/'
--exclude='*'
```
(the exclude part is needed)
## bluetooth pair debian
```
	sudo apt-get install blueman
	restart pc
	in bluetooth icon in panel:
		devices → select srs-xb22 → setup → headset 
```
## boot priority change grub
```
	sudo subl /etc/default/grub
	sudo update-grub
```
## boot loader add windows to grub list fix
```
One drastic ‘solution’ is to rename the manjaro efi file as the windows efi file. First move the windows file to a higher directory before doing so just in case we mess up so we can revert to the same situation.
At Manjaro terminal
sudo cp /boot/efi/EFI/Microsoft/Boot/bootmgfw.efi /boot/efi/EFI/Microsoft/bootmgfw.efi
sudo cp /boot/grub/x86_64-efi/core.efi /boot/efi/EFI/boot/bootx64.efi
sudo cp /boot/grub/x86_64-efi/core.efi /boot/efi/EFI/Microsoft/Boot/bootmgfw.efi
Reboot and it should boot to the Manjaro grub menu. Boot up Manjaro and do
sudo update-grub
If that does not boot windows (possible because you moved windows efi file up a directory), Then we’ll add a custom entry for windows in our grub menu.
At Manjaro terminal
sudo touch /boot/grub/custom.cfg
Then open up that file /boot/grub/custom.cfg
and add following to it and then save the file. That’s it. No need to ‘update-grub’. It won’t appear in grub.cfg or os-prober but entry will be there (at bottom of grub menu) when you reboot.

menuentry "Windows New directory" {
        insmod part_gpt
        insmod fat
        insmod chain
        search --no-floppy --fs-uuid --set=3A21-0848
        chainloader /EFI/Microsoft/bootmgfw.efi
}
https://forum.manjaro.org/t/grub-not-booting-unless-i-manually-boot-it-from-live-cd/43547/3
https://forum.manjaro.org/t/grub-not-showing-up-on-uefi-pc/43647/8
```
## boot loader point to REFIND after windows update
```
	on Windows open cmd as admin
	bcdedit /set {bootmgr} path \efi\refind\refind_x64.efi
```
## boot loader point to manjaro after windows update
```
	on Windows open cmd as admin
	bcdedit /set {bootmgr} path \efi\Manjaro\grubx64.efi
	https://itsfoss.com/no-grub-windows-linux/
```
## boot loader point to manjaro after windows update
```
	look up
	if the above does not work check these
	https://wiki.manjaro.org/index.php/Restore_the_GRUB_Bootloader
	https://askubuntu.com/questions/831216/how-can-i-reinstall-grub-to-the-efi-partition
	https://forum.manjaro.org/t/grub-not-booting-unless-i-manually-boot-it-from-live-cd/43547/3
	https://forum.manjaro.org/t/grub-not-showing-up-on-uefi-pc/43647/8
	https://www.rootandadmin.com/index.php/2016/04/06/comment-reparer-lefi-bootloader-dans-windows-10/?lang=en

```
## check if command/program/script is supported by a package
```
	apt-file search <name of command>
```
## concat pdf files
```
	pdftk p1.pdf p2.pdf cat output output.pdf
```
## convert flv 2 mp3
```
	ffmpeg -i inpuvvideofile.flv -ab 128k outputaudiofile.mp3
or
pacpl --to mp3 "video_name.flv"
```
## convert rmvb 2 avi:
```
mencoder foo.rmvb -o foo.avi -ovc xvid -xvidencopts fixed_quant=5 -oac mp3lame -lameopts abr:br=128
```
## convert mkv 2 avi:
```
avconv -i "/home/me/Desktop/transform_me/House.of.Cards.S01.Ep13.1080p.BluRay.x265-n0m1.mkv" -f avi -c:v mpeg4 -b:v 4000k -c:a libmp3lame -b:a 320k "/home/me/Desktop/transform_me/House.of.Cards.S01.Ep13.1080p.BluRay.x265-n0m1.avi"
```
## copy img to sd card
```
sudo dd bs=4M if=/home/me/Downloads/OpenELEC-RPi.arm-5.0.8.img of=/dev/sdc
```
## copy sd card to hard disk
```
sudo dd bs=4M if=/dev/sdb of=/media/me/todos/rasberry_stuf/perfect_openelec.img
```
## copy terminal output to file.txt
```
	command | tee file.txt
```
## download song/playlist from youtube
```
	youtube-dl -x --audio-format mp3 --audio-quality 0 www.youtubelink
```
## [fix slow start up for applications in manjaro after update (caused by gnome44)](https://forum.manjaro.org/t/very-slow-startup-on-some-apps-after-update/141806)
```
sudo pacman -Rdd xdg-desktop-portal-gnome
sudo pacman -S xdg-desktop-portal-gtk
```
## [fix target not found on pamac updates (seen on aur)](https://forum.manjaro.org/t/need-help-with-error-latest-update/143967)
```
sudo pacman-mirrors --geoip
sudo pacman -Syyu
```
## fix aur updates failed to sync with db in pamac UI
```
preferences
Third party
toggle check for updates
apply updates (this would install only the non-aur updates)
toggle check for updates
apply updates (this would hopefully install the aur updates this time)
``` 
## exif data copy from don2.JPG to others in same folder
```
	jhead -te "don2.JPG" *.jpg
```
## kernel panic/freeze etc will be handled by kdump. To see logs
```
	sudo crash /usr/lib/debug/vmlinux-`uname -r` \
	/var/crash/201707022002/dump.201707022002
```
## laptop lid close to leave external display open
```
	in /etc/UPower/UPower.conf to set
	IgnoreLid=true
```
## merge pdfs:
```
	pdfsam or
	pdfmerge *.pdf outname.pdf
```
## mobile Internet Cosmote settings
```
Πηγαινε στις ρυθμισεις εδω Settings –> Wireless controls –> Mobile 
networks (αν εχεις ελληνικα κανε την μεταφραση) και δες αν υπαρχουν τα παρακατω:
Όνομα Cosmote Wireless Internet
APN Internet
MCC 202
MNC 01
Τύπος APN default
```
## monitor screen fix (two screens) for xfce
```
	xrandr --output eDP1 --mode 1360x768 --primary --output HDMI1 	--auto --left-of eDP1
```
## recent files deletion debian
```
	rm ~/.local/share/recently-used.xbel
	touch ~/.local/share/recently-used.xbel
```
## redshift for warm screen colors at night (for AMS)
```
	redshift -l 52.22:4.53
```
## redshift to autostart
```
	/home/me/.config/autostart/redshift.desktop
```
## redshift to stop
```
	sudo pgrep redshift | xargs kill
```
## remove pass from suspend
```
gconf-editor/app/gnome-power-manager/lock/suspend
```
## set docker file limit to avoid all memory be consumed by mysql 5.7 container
```
sudo systemctl stop docker
sudo dockerd --default-ulimit nofile=262144:262144
```
this needs to be done on every boot
original open files limit for mysql 5.7 container:
```
docker run -ti 09361feeb475 /bin/bash
ulimit -a
```
included this line:
```open files                      (-n) 1073741816```
## share screen on teams web via chromium is sway fix:
```
sudo pacman -S xdg-desktop-portal-wlr
```
then rebood
## shotwell backup 
```
If you want to copy your Shotwell library to a different computer, it's a simple three step process:
    1. Copy the ~/.shotwell directory
    2. Copy your photo library or "Pictures" directory (see note below.)
    3. Start Shotwell on the new computer, open Edit->Preferences and 	make sure the pictures directory is correct.
Make sure to copy all subfolders of both the Pictures directory and the ~/.shotwell directory.
```
## stop suspend on gnome desktop
create a file with and endless loop
```
#!/bin/bash
while :
do
	echo "Press [CTRL+C] to stop.."
	sleep 1
done
```
make it executable:
```
sudo chmod +x ~/Downloads/loop
```
run it with session inhibit prefix:
```
gnome-session-inhibit --inhibit idle ~/Downloads/loop
```
## suspend fix on debian xfce
```
	power manager > security > untick lock screen when system is going 	to sleep 
```
## torrent share files with friends
```
	deluge while creating torrent make sure to:
	add tracker ie udp://tracker.coppersurfer.tk:6969/announce
	add this torrent to the session (options tab)
```
## torrent share trackerless with transmission → deluge
```
	transmission/settings/network/use local peer discovery to find more 	peers
	create torrent
	share torrent file
```
## usb format
```
	sudo mkfs.vfat -n 'FLASKI' -I /dev/sdc
```
## video editing
```
	ffmpeg -i movie.avi -ss 00:51:42 -t 00:03:30 clip.avi
This will get you the desired clip. Now let me explain you the commands in short:
-i movie.avi - this is the input file
-ss hh:mm:ss - this is the time at which the extracted clip starts
-t hh:mm:ss - this is the duration of the clip
clip.avi - this is the output file

```
## vscode solargraph fix
```
	cd to project directory
	bundle install (after removing image magic from gemfile)
	bundle exec gem install solargraph
	gem install bundler:1.17.3
```
## window border removal for xfce
```
	mkdir -p /usr/share/themes/empty/xfwm4
	touch /usr/share/themes/empty/xfwm4/themer
	then select empty from window manager
```
## 7zip add password and create zip
```
	7z a <output_name.7z> <directory_to_archive> -p -mhe
```
